//
// Created by desrumaux on 17/04/2020.
//

#ifndef REACTOR_BADVALUEEXCEPTION_HPP
#define REACTOR_BADVALUEEXCEPTION_HPP

#include <stdexcept>

namespace Reactor {

    namespace Exception {

        class BadValueException : public std::runtime_error {
        public:

            /**
             * @brief This error cover a wrong type exception on casting
             *
             * @param message
             */
            BadValueException( const std::string message );

            /**
             * @brief destructor of bad cast cast exception
             */
            virtual ~BadValueException() throw();

        };

    }

}

#endif //REACTOR_BADVALUEEXCEPTION_HPP
