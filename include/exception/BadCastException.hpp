//
// Created by desrumaux on 08/04/2020.
//

#ifndef REACTOR_BADCASTEXCEPTION_HPP
#define REACTOR_BADCASTEXCEPTION_HPP

#include <stdexcept>

namespace Reactor {

    namespace Data {
        class Object;
    }

    namespace Exception {

        class BadCastException : public std::runtime_error {
        public:

            /**
             * @brief This error cover a wrong type exception on casting the object
             *
             * @param object
             * @param expectedType
             */
            BadCastException( const Data::Object& object, const std::string expectedType );

            /**
             * @brief This error cover a wrong type exception on casting
             *
             * @param message
             */
            BadCastException( const std::string message );

            /**
             * @brief destructor of bad cast cast exception
             */
            virtual ~BadCastException() throw();
        };

    }

}

#endif //REACTOR_BADCASTEXCEPTION_HPP
