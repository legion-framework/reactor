//
// Created by desrumaux on 16/04/2020.
//

#ifndef REACTOR_BADCONVERSIONEXCEPTION_HPP
#define REACTOR_BADCONVERSIONEXCEPTION_HPP

#include <stdexcept>

namespace Reactor {

    namespace Data {
        class Object;
    }

    namespace Exception {

        class BadConversionException : public std::runtime_error {
        public:

            /**
             * @brief This error cover a wrong conversion exception
             *
             * @param value
             * @param expectedValue
             */
            BadConversionException( const Data::Object& value, const std::string expectedValue );

            /**
             * @brief This error cover a wrong conversion exception
             *
             * @param value
             * @param expectedValue
             */
            BadConversionException( const std::string value, const std::string expectedValue );

            /**
             * @brief This error cover a wrong type exception on casting
             *
             * @param message
             */
            BadConversionException( const std::string message );

            /**
             * @brief destructor of bad cast cast exception
             */
            virtual ~BadConversionException() throw();
        };

    }

}

#endif //REACTOR_BADCONVERSIONEXCEPTION_HPP
