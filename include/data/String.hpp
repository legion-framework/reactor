//
// Created by desrumaux on 10/04/2020.
//

#ifndef REACTOR_STRING_HPP
#define REACTOR_STRING_HPP

#include <string>

#include "TemplateObject.hpp"

namespace Reactor {

    namespace Data {

        class String : public Object, public std::string {
        public:

            /**
             * @brief constructor
             *
             * @param value
             */
            String(const std::string value);

            /**
             * @brief copy constructor
             *
             * @param value
             */
            String(const String& value);

            /**
             * @brief destructor
             */
            virtual ~String();

            /**
             * @return a description of an object
             */
            virtual std::string toString() const;

            /**
             * @brief convert the string to lowercase
             *
             * @return a lowered string
             */
            String toLower() const;

            /**
             * @brief convert the string to lowercase
             *
             * @param string
             *
             * @return a lowered string
             */
            static std::string toLower(const std::string& string);

            /**
             * @brief convert a string to a escaped one
             *
             * @return a escaped string
             */
            String escape() const;

            /**
             * @brief convert a string to a escaped one
             *
             * @param string
             *
             * @return a escaped string
             */
            static std::string escape(const std::string& string);

            /**
             * @brief quote the string
             *
             * @return a quoted string
             */
            String quote() const;

            /**
             * @brief quote the string
             *
             * @return a quoted string
             */
            static std::string quote(const std::string& string);

        };

    }

}

#endif //REACTOR_STRING_HPP
