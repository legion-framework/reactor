//
// Created by desrumaux on 08/04/2020.
//

#ifndef REACTOR_OBJECT_HPP
#define REACTOR_OBJECT_HPP

#include <string>
#include <sstream>

#include "exception/BadCastException.hpp"

namespace Reactor {

    namespace Data {

        class Object {
        public:

            /**
             * @brief the object is a boolean one
             *
             * @return true or false
             */
            template <typename Type>
            bool is() const
            {
                return dynamic_cast<const Type*>(this) != nullptr;
            }

            /**
             * @brief cast the object to a another type
             *
             * @return cast to a boolean
             */
            template <typename Type>
            Type& as()
            {
                Type* object = dynamic_cast<Type*>(this);

                if (!is<Type>() || object == nullptr) {
                    throw Exception::BadCastException(*this, "Boolean");
                }

                return *object;
            }

            /**
             * @brief cast the object to a another type
             *
             * @return cast to a boolean
             */
            template <typename Type>
            const Type& as() const
            {
                const Type* object = dynamic_cast<const Type*>(this);

                if (!is<Type>() || object == nullptr) {
                    throw Exception::BadCastException(*this, "Boolean");
                }

                return *object;
            }


            /**
             * @return a description of an object
             */
            virtual std::string toString() const;

        };

    }

}

#endif //REACTOR_OBJECT_HPP
