//
// Created by desrumaux on 10/04/2020.
//

#ifndef REACTOR_TEMPLATENUMBER_HPP
#define REACTOR_TEMPLATENUMBER_HPP

#include "Number.hpp"
#include "TemplateObject.hpp"

namespace Reactor {

    namespace Data {

        template <typename Type>
        class TemplateNumber : public Number, public TemplateObject<Type> {
        public:

            /**
             * @brief Constructor
             *
             * @param value
             */
            TemplateNumber(const Type value)
                : TemplateObject<Type>(value)
            {}

            /**
             * @brief copy constructor
             *
             * @param value
             */
            TemplateNumber(const TemplateNumber<Type>& value)
                : TemplateObject<Type>(value)
            {}

            /**
             * @brief destructor
             */
            virtual ~TemplateNumber()
            {}

            /**
             * @return the number as a double
             */
            virtual double getDouble() const
            {
                return (double) this->value;
            }

            /**
             * @return the number as a integer
             */
            virtual int getInt() const
            {
                return (int) this->value;
            }

            /**
             * @return the number as a string
             */
            virtual std::string toString() const override
            {
                std::ostringstream oss;
                oss << this->value;
                return oss.str();
            }

        };

    }

}

#endif //REACTOR_TEMPLATENUMBER_HPP
