//
// Created by desrumaux on 10/04/2020.
//

#ifndef REACTOR_NUMBER_HPP
#define REACTOR_NUMBER_HPP

#include "Object.hpp"

namespace Reactor {

    namespace Data {

        class Number : public virtual Object {
        public:

            /**
             * @return the number as a double
             */
            virtual double getDouble() const = 0;

            /**
             * @return the number as a integer
             */
            virtual int getInt() const = 0;

        };

    }

}

#endif //REACTOR_NUMBER_HPP
