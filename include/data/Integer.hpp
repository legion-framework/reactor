//
// Created by desrumaux on 10/04/2020.
//

#ifndef REACTOR_INTEGER_HPP
#define REACTOR_INTEGER_HPP

#include "TemplateNumber.hpp"

namespace Reactor {

    namespace Data {

        class Integer : public TemplateNumber<int> {
        public:

            /**
             * @brief constructor
             *
             * @param value
             */
            Integer(const int value);

            /**
             * @brief copy constructor
             *
             * @param value
             */
            Integer(const Integer& value);

            /**
             * @brief destructor
             */
            virtual ~Integer();

            /**
             * @brief Parse the number
             *
             * @param string
             * @return
             */
            static Integer fromString(const std::string& string);

        };

    }

}

#endif //REACTOR_INTEGER_HPP
