//
// Created by desrumaux on 10/04/2020.
//

#ifndef REACTOR_ARRAY_HPP
#define REACTOR_ARRAY_HPP

#include <vector>

#include "Object.hpp"
#include "Nullable.hpp"

namespace Reactor {

    namespace Data {

        class Array : public Object, public std::vector<Nullable> {
        public:

            /**
             * @return a description of an object
             */
            virtual std::string toString() const override;

        };

    }

}

#endif //REACTOR_ARRAY_HPP
