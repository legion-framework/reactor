//
// Created by desrumaux on 08/04/2020.
//

#ifndef REACTOR_BOOLEAN_HPP
#define REACTOR_BOOLEAN_HPP

#include "data/TemplateObject.hpp"

namespace Reactor {

    namespace Data {

        class Boolean : public TemplateObject<bool> {
        public:

            /**
             * @brief constructor
             *
             * @param value
             */
            Boolean(bool value);

            /**
             * @brief copy constructor
             *
             * @param boolean
             */
            Boolean(const Boolean& boolean);

            /**
             * @brief destructor
             */
            virtual ~Boolean();

            /**
             * @brief Parse the number
             *
             * @param string
             * @return
             */
            static Boolean fromString(const std::string& string);

        };

    }

}

#endif //REACTOR_BOOLEAN_HPP
