//
// Created by desrumaux on 10/04/2020.
//

#ifndef REACTOR_STRUCTURE_HPP
#define REACTOR_STRUCTURE_HPP

#include <map>
#include <string>
#include <memory>
#include <sstream>

#include "Object.hpp"
#include "Nullable.hpp"

namespace Reactor {

    namespace Data {

        using StructureKey = std::string;
        using StructureValue = Nullable;

        class Structure : public Object, public std::map<StructureKey, StructureValue> {
        public:

            /**
             * @return a description of an object
             */
            virtual std::string toString() const override;

        };

    }

}

#endif //REACTOR_STRUCTURE_HPP
