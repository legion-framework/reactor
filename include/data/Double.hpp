//
// Created by desrumaux on 10/04/2020.
//

#ifndef REACTOR_DOUBLE_HPP
#define REACTOR_DOUBLE_HPP

#include "TemplateNumber.hpp"

namespace Reactor {

    namespace Data {

        class Double : public TemplateNumber<double> {
        public:

            /**
             * @brief constructor
             *
             * @param value
             */
            Double(const double value);

            /**
             * @brief copy constructor
             *
             * @param value
             */
            Double(const Double& value);

            /**
             * @brief destructor
             */
            virtual ~Double();

            /**
             * @brief Parse the number
             *
             * @param string
             * @return
             */
            static Double fromString(const std::string& string);

        };

    }

}

#endif //REACTOR_DOUBLE_HPP
