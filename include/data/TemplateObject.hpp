//
// Created by desrumaux on 10/04/2020.
//

#ifndef REACTOR_TEMPLATEOBJECT_HPP
#define REACTOR_TEMPLATEOBJECT_HPP

#include "Object.hpp"

namespace Reactor {

    namespace Data {

        template <typename Type>
        class TemplateObject : public Object {
        protected:

            /**
             * @brief It's the instance of the value
             */
            Type value;

        public:

            /**
             * @brief create a templated object
             *
             * @param value
             */
            TemplateObject(const Type value)
                : value(value)
            {}

            /**
             * @brief create a templated object
             *
             * @param value
             */
            TemplateObject(const TemplateObject<Type>& value)
                : value(value.value)
            {}

            /**
             * @brief destructor
             */
            virtual ~TemplateObject()
            {}

            /**
             * @brief get the holded value
             *
             * @return the value
             */
            Type getValue() const
            {
                return value;
            }

            /**
             * @brief set the holded value
             *
             * @param value
             */
            void setValue(const Type value)
            {
                this->value = value;
            }

        };

    }

}

#endif //REACTOR_TEMPLATEOBJECT_HPP
