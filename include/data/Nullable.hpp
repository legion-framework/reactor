//
// Created by desrumaux on 17/04/2020.
//

#ifndef REACTOR_NULLABLE_HPP
#define REACTOR_NULLABLE_HPP

#include <memory>

#include "Object.hpp"

namespace Reactor {

    namespace Data {

        class Nullable : public Object, public std::unique_ptr<Object> {
        public:

            /**
             * @brief constructor
             *
             * @param value
             */
            Nullable(Object* value = nullptr);

            /**
             * @brief copy constructor
             *
             * @param nullable
             */
            Nullable(Nullable& nullable);

            /**
             * @brief destructor
             */
            virtual ~Nullable();

            /**
             * @brief The object is null or not
             *
             * @return a boolean
             */
            bool isNull() const;

            /**
             * @return a description of an object
             */
            virtual std::string toString() const;

        };

    }

}

#endif //REACTOR_NULLABLE_HPP
