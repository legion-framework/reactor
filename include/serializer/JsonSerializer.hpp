//
// Created by desrumaux on 16/04/2020.
//

#ifndef REACTOR_JSONSERIALIZER_HPP
#define REACTOR_JSONSERIALIZER_HPP

#include <ostream>

#include "ISerializer.hpp"
#include "data/Structure.hpp"
#include "data/Array.hpp"
#include "data/Number.hpp"
#include "data/String.hpp"
#include "data/Nullable.hpp"
#include "data/Array.hpp"

namespace Reactor {

    namespace Serializer {

        class JsonSerializer : public ISerializer {
        protected:

            /**
             * @brief It's the input streaming
             */
            std::ostream& output;

        public:

            /**
             * @brief constructor
             *
             * @param output
             */
            JsonSerializer(std::ostream& output);

            /**
             * @brief disallow copy constructor
             *
             * @param serializer
             */
            JsonSerializer(const JsonSerializer& serializer) = delete;

            /**
             * @brief destructor
             */
            virtual ~JsonSerializer();

            /**
             * @brief add the number to the flux
             *
             * @param number
             *
             * @return itself
             */
            JsonSerializer& operator<<(const Data::Number& number);

            /**
             * @brief add the structure to the flux
             *
             * @param structure
             *
             * @return itself
             */
            JsonSerializer& operator<<(const Data::Structure& structure);

            /**
             * @brief add the string to the flux
             *
             * @param string
             *
             * @return itself
             */
            JsonSerializer& operator<<(const Data::String& string);

            /**
             * @brief add the string to the flux
             *
             * @param nullable
             *
             * @return itself
             */
            JsonSerializer& operator<<(const Data::Nullable& nullable);

            /**
             * @brief add the array to the flux
             *
             * @param array
             *
             * @return itself
             */
            JsonSerializer& operator<<(const Data::Array& array);

            /**
             * @brief add the object to the flux
             *
             * @param object
             *
             * @return itself
             */
            JsonSerializer& operator<<(const Data::Object& object);

        };

    }

}

#endif //REACTOR_JSONSERIALIZER_HPP
