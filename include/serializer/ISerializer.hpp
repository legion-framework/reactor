//
// Created by desrumaux on 10/04/2020.
//

#ifndef REACTOR_ISERIALIZER_HPP
#define REACTOR_ISERIALIZER_HPP

#include <string>

#include "data/Object.hpp"

namespace Reactor {

    namespace Serializer {

        class ISerializer {
        public:

            /**
             * @brief add the objet to the flux
             *
             * @param object
             *
             * @return itself
             */
            virtual ISerializer& operator<<(const Data::Object& object) = 0;

        };

    }

}

#endif //REACTOR_ISERIALIZER_HPP
