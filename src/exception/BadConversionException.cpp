//
// Created by desrumaux on 16/04/2020.
//

#include "exception/BadConversionException.hpp"
#include "data/Object.hpp"

namespace Reactor {

    namespace Exception {

        /**
         * @brief This error cover a wrong conversion exception
         *
         * @param value
         * @param expectedValue
         */
        BadConversionException::BadConversionException( const Data::Object& value, const std::string expectedValue )
            : std::runtime_error( "Bad conversion given: " + value.toString() + ", expected: " + expectedValue )
        {}

        /**
         * @brief This error cover a wrong conversion exception
         *
         * @param value
         * @param expectedValue
         */
        BadConversionException::BadConversionException( const std::string value, const std::string expectedValue )
            : std::runtime_error( "Bad conversion given: " + value + ", expected: " + expectedValue )
        {}

        /**
         * @brief This error cover a wrong type exception on casting
         *
         * @param message
         */
        BadConversionException::BadConversionException( const std::string message )
            : std::runtime_error(message)
        {}

        /**
         * @brief destructor of bad cast cast exception
         */
        BadConversionException::~BadConversionException() throw()
        {}

    }

}
