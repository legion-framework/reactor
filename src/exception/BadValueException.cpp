//
// Created by desrumaux on 17/04/2020.
//

#include "exception/BadValueException.hpp"

namespace Reactor {

    namespace Exception {


        /**
         * @brief This error cover a wrong type exception on casting
         *
         * @param message
         */
        BadValueException::BadValueException(const std::string message)
            : std::runtime_error(message)
        {}

        /**
         * @brief destructor of bad cast cast exception
         */
        BadValueException::~BadValueException() throw()
        {}

    }

}