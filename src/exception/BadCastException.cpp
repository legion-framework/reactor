//
// Created by desrumaux on 08/04/2020.
//

#include "exception/BadCastException.hpp"
#include "data/Object.hpp"

namespace Reactor {

    namespace Exception {

        /**
         * @brief This error cover a wrong type exception on casting the object
         *
         * @param object
         * @param expectedType
         */
        BadCastException::BadCastException( const Data::Object& object, const std::string expectedType )
            : std::runtime_error( "Bad cast exception " + object.toString() + " given but " + expectedType + " expected."  )
        {}

        /**
         * @brief This error cover a wrong type exception on casting
         *
         * @param message
         */
        BadCastException::BadCastException( const std::string message )
            : std::runtime_error( message )
        {

        }

        /**
         * @brief destructor of bad cast cast exception
         */
        BadCastException::~BadCastException() throw()
        {}

    }

}