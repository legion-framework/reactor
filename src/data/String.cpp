//
// Created by desrumaux on 10/04/2020.
//

#include "data/String.hpp"

namespace Reactor {

    namespace Data {

        /**
         * @brief constructor
         *
         * @param value
         */
        String::String(const std::string value)
            : std::string(value)
        {}

        /**
         * @brief copy constructor
         *
         * @param value
         */
        String::String(const String& value)
            : std::string(value)
        {}

        /**
         * @brief destructor
         */
        String::~String()
        {}

        /**
         * @return the string it self
         */
        std::string String::toString() const
        {
            return std::string(*this);
        }

        /**
         * @brief convert the string to lowercase
         *
         * @return a lowered string
         */
        String String::toLower() const
        {
            return String{ String::toLower(*this )};
        }

        /**
         * @brief convert the string to lowercase
         *
         * @param string
         *
         * @return a lowered string
         */
        std::string String::toLower(const std::string& string)
        {
            std::string result;
            result.resize(string.size());

            for (char c : string){
                if (c >= 'A' && c <= 'Z') {
                    result += (c - 'A');
                } else {
                    result += c;
                }
            }

            return result;
        }

        /**
         * @brief convert a string to a escaped one
         *
         * @return a escaped string
         */
        String String::escape() const
        {
            return String{String::escape(*this)};
        }

        /**
         * @brief convert a string to a escaped one
         *
         * @param string
         *
         * @return a escaped string
         */
        std::string String::escape(const std::string& string)
        {
            std::string result = "";

            for(const char& c : string){
                if (c == '\n') {
                    result += "\\n";
                } else if (c == '\t') {
                    result += "\\t";
                } else if (c == '\b') {
                    result += "\\b";
                } else if (c == '\r') {
                    result += "\\r";
                } else {
                    result += c;
                }
            }

            return result;
        }

        /**
         * @brief quote the string
         *
         * @return a quoted string
         */
        String String::quote() const
        {
            return String{String::quote(*this)};
        }

        /**
         * @brief quote the string
         *
         * @return a quoted string
         */
        std::string String::quote(const std::string& string)
        {
            std::string result = "\"";

            for(const char& c : string){
                if (c == '"') {
                    result += '\\"';
                } else {
                    result += c;
                }
            }

            return result + '"';
        }

    }

}