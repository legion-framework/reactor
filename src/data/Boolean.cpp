//
// Created by desrumaux on 08/04/2020.
//
#include "data/Boolean.hpp"
#include "data/String.hpp"

#include "exception/BadConversionException.hpp"

namespace Reactor {

    namespace Data {

        /**
         * @brief constructor
         *
         * @param value
         */
        Boolean::Boolean(bool value)
            : TemplateObject<bool>(value)
        {}

        /**
         * @brief copy constructor
         *
         * @param boolean
         */
        Boolean::Boolean(const Boolean& boolean)
            : TemplateObject<bool>(boolean)
        {}

        /**
         * @brief destructor
         */
        Boolean::~Boolean()
        {}

        /**
         * @brief Parse the number
         *
         * @param string
         * @return
         */
        Boolean Boolean::fromString(const std::string& string)
        {
            std::string value = String::toLower(string);

            if (value == "yes" || value == "true") {
                return true;
            } else if (value == "no" || value == "false") {
                return false;
            } else {
                throw Exception::BadConversionException(value, "yex, no, true, false");
            }

        }

    }

}