//
// Created by desrumaux on 17/04/2020.
//

#include "data/Array.hpp"

namespace Reactor {

    namespace Data {


        /**
         * @return a description of an object
         */
        std::string Array::toString() const {
            std::stringstream ss;
            ss << Object::toString() << "[size:" << this->size() << "]";
            return ss.str();
        }

    }

}