//
// Created by desrumaux on 08/04/2020.
//
#include "data/Object.hpp"
#include "exception/BadCastException.hpp"

#include <typeinfo>

namespace Reactor {

    namespace Data {

        /**
         * @return a description of an object
         */
        std::string Object::toString() const
        {
            std::stringstream ss;
            ss << typeid(this).name() << "@" << this;
            return ss.str();
        }

    }

}