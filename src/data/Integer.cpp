//
// Created by desrumaux on 10/04/2020.
//

#include "data/Integer.hpp"

namespace Reactor {

    namespace Data {

        /**
         * @brief constructor
         *
         * @param value
         */
        Integer::Integer(const int value)
            : TemplateNumber<int>(value)
        {}

        /**
         * @brief copy constructor
         *
         * @param value
         */
        Integer::Integer(const Integer& value)
            : TemplateNumber<int>(value)
        {}

        /**
         * @brief destructor
         */
        Integer::~Integer()
        {}

        /**
         * @brief Parse the number
         *
         * @param string
         * @return
         */
        Integer Integer::fromString(const std::string& string)
        {
            return Integer{ std::stoi(string) };
        }

    }

}