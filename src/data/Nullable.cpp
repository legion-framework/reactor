//
// Created by desrumaux on 17/04/2020.
//

#include "data/Nullable.hpp"

namespace Reactor {

    namespace Data {

        /**
         * @brief constructor
         *
         * @param value
         */
        Nullable::Nullable(Object* value)
            : std::unique_ptr<Object>(value)
        {}

        /**
         * @brief copy constructor
         *
         * @param nullable
         */
        Nullable::Nullable(Nullable& nullable)
            : std::unique_ptr<Object>(nullable.release())
        {}

        /**
         * @brief destructor
         */
        Nullable::~Nullable()
        {}

        /**
         * @brief The object is null or not
         *
         * @return a boolean
         */
        bool Nullable::isNull() const
        {
            return this->get() == nullptr;
        }

        /**
         * @return a description of an object
         */
        std::string Nullable::toString() const
        {
            if (isNull()) {
                return "null";
            }

            return this->get()->toString();
        }

    }

}
