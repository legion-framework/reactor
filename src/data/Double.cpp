//
// Created by desrumaux on 10/04/2020.
//

#include "data/Double.hpp"

namespace Reactor {

    namespace Data {

        /**
         * @brief constructor
         *
         * @param value
         */
        Double::Double(const double value)
            : TemplateNumber<double>(value)
        {}

        /**
         * @brief copy constructor
         *
         * @param value
         */
        Double::Double(const Double& value)
            : TemplateNumber<double>(value)
        {}

        /**
         * @brief destructor
         */
        Double::~Double()
        {}

        /**
         * @brief Parse the number
         *
         * @param string
         * @return
         */
        Double Double::fromString(const std::string& string)
        {
            return Double { std::stod(string) };
        }

    }

}
