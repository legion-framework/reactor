//
// Created by desrumaux on 10/04/2020.
//

#include "data/Structure.hpp"

namespace Reactor {

    namespace Data {

        /**
         * @return a description of an object
         */
        std::string Structure::toString() const
        {
            std::stringstream ss;
            ss << Object::toString() << "[size:" << this->size() << "]";
            return ss.str();
        }

    }

}
