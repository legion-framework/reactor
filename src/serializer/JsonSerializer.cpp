//
// Created by desrumaux on 17/04/2020.
//

#include "serializer/JsonSerializer.hpp"
#include "exception/BadValueException.hpp"

namespace Reactor {

    namespace Serializer {

        /**
         * @brief constructor
         *
         * @param output
         */
        JsonSerializer::JsonSerializer(std::ostream& output)
            : output(output)
        {}

        /**
         * @brief destructor
         */
        JsonSerializer::~JsonSerializer()
        {}

        /**
         * @brief add the number to the flux
         *
         * @param number
         *
         * @return itself
         */
        JsonSerializer& JsonSerializer::operator<<(const Data::Number& number)
        {
            output << number.toString();
            return *this;
        }

        /**
         * @brief add the structure to the flux
         *
         * @param structure
         *
         * @return itself
         */
        JsonSerializer& JsonSerializer::operator<<(const Data::Structure& structure)
        {
            output << '{';

            bool first = true;
            for(auto it = structure.begin();
                it != structure.end();
                it++) {

                if (!first) {
                    output << ',';
                } else {
                    first = false;
                }

                output << '"' << it->first << "\":";
                this->operator<<(it->second);

                output << '}';
            }

            return *this;
        }

        /**
         * @brief add the string to the flux
         *
         * @param string
         *
         * @return itself
         */
        JsonSerializer& JsonSerializer::operator<<(const Data::String& string){
            output << string.escape().quote();
            return *this;
        }

        /**
         * @brief add the string to the flux
         *
         * @param nullable
         *
         * @return itself
         */
        JsonSerializer& JsonSerializer::operator<<(const Data::Nullable& nullable)
        {
            if (nullable.isNull()) {
                output << "null";
            } else {
                this->operator<<(*nullable);
            }

            return *this;
        }

        /**
         * @brief add the array to the flux
         *
         * @param array
         *
         * @return itself
         */
        JsonSerializer& JsonSerializer::operator<<(const Data::Array& array)
        {
            output << '[';
            bool first = true;
            for(const auto& nullable : array) {
                if (!first) {
                    output << ',';
                } else {
                    first = false;
                }

                this->operator<<(nullable);
            }
            output << ']';

            return *this;
        }

        /**
         * @brief add the object to the flux
         *
         * @param object
         *
         * @return itself
         */
        JsonSerializer& JsonSerializer::operator<<(const Data::Object& object)
        {
            if (object.is<Data::Number>()) {
                this->operator<<(object.as<Data::Number>());
            } else if (object.is<Data::Nullable>()) {
                this->operator<<(object.as<Data::Nullable>());
            } else if (object.is<Data::String>()) {
                this->operator<<(object.as<Data::String>());
            } else if (object.is<Data::Structure>()) {
                this->operator<<(object.as<Data::Structure>());
            } else if (object.is<Data::Array>()) {
                this->operator<<(object.as<Data::Array>());
            } else {
                throw Exception::BadValueException("The object is not a structure, an array, a number or a string");
            }

            return *this;
        }

    }

}
